﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ecommerce.Models;

namespace Ecommerce.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Business Bo;
        public static  string pno;
        public HomeController(ILogger<HomeController> logger, Business Bo)
        {
            _logger = logger;
            this.Bo = Bo;
        }

        public IActionResult Index()
        {
            var rec = Bo.GetProducts();
            var catlist = Bo.GetCategories();
            ViewBag.CategoryList = catlist;
            return View(rec);
        }
        [HttpPost]
        public IActionResult SaveUserByMobileNumber(string MobileNumber)
        {
            pno = MobileNumber;
            if ((pno.Contains(" ")) || (pno.Contains("-")))
            {
                pno = pno.Replace(" ", "");
                pno = pno.Replace("-", "");
            }
            var success = -1;
            try
            {
                if (string.IsNullOrEmpty(MobileNumber))
                {
                    return Json("Invalid Mobile Number");
                }
                success = Bo.SaveUserByMobileNumber(MobileNumber);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }

            return Json(success);
        }
        public IActionResult Login(string role)
        {
            ViewBag.Role = role;
            return View();
        }
        [HttpPost]
        public IActionResult Addtocart(string outs)
        {
            var success = -1;
            try
            {
                if (string.IsNullOrEmpty(outs))
                {
                    return Json("Invalid");
                }
                success = Bo.addToCart(outs, pno);
                return Json(success);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        [HttpPost]
        public IActionResult UpdateStatus()
        {
            var upd = -1;
            string OTP = null;
            try
            {
                var rec = Bo.UpdateStatus(pno);
                upd = rec.Item1;
                OTP = rec.Item2;
                
                //return Json(upd);
                return new JsonResult(new { success = upd,otp= OTP });
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
        public IActionResult Authenticate(string UserName, string Password)
        {
            var isUserExist = Bo.ValidateUser(UserName,Password);
            if(isUserExist == 1)
            {
                return RedirectToAction("Index", "Admin");
            }
            return View("Login");
        }
        public IActionResult AuthenticateSalesPerson(string UserName, string Password)
        {
            var isUserExist = Bo.ValidateSalesPerson(UserName, Password);
            if (isUserExist == 1)
            {
                return RedirectToAction("Index", "Sales");
            }
            return View("Login");
        }
        public IActionResult TrackOrder(string otp)
        {
            var rec = Bo.GetProductByOtp(otp);
            if(rec != null && rec.Count > 0)
            {
                return Json(rec.First().Status);
            }
            else
            {
                return Json("No Product Found");
            }

        }
    }
}
