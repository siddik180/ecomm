﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Controllers
{
    public class SalesController : Controller
    {
        private readonly Business Bo;
        public SalesController(Business Bo)
        {
            this.Bo = Bo;
        }
        public IActionResult Index()
        {
            var rec = Bo.GetOutOfOrderProduct();
            ViewBag.OutForODelivery = rec;
            return View();
        }
        [HttpPost]
        public IActionResult UpdateSataus(string otp, string status)
        {

            try
            {
                var isUpdated = Bo.UpdateStausByAdmin(otp, status);
                if (isUpdated > 0)
                {
                    return Json("Success");
                }
                else
                {
                    return Json("Some thing Went Wrong");
                }
            }
            catch (Exception e)
            {

                return Json(e.Message);
            }

        }
        public IActionResult GertProductByOTP(string otp)
        {
            var list = Bo.GetProductByOtp(otp);
            ViewBag.role = "sales";
            return View("StatusView", list);
        }
    }
}
