﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Ecommerce.Controllers
{
    public class AdminController : Controller
    {

        private readonly Business Bo;
        public AdminController(Business Bo)
        {
            this.Bo = Bo;
        }
        public IActionResult Index()
        {
            var rec = Bo.GetOrderedProduct();
            ViewBag.OrderedProduct = rec;
            return View();
        }
        [HttpPost]
        public IActionResult UpdateSataus(string otp, string status)
        {
           
            try
            {
                var isUpdated = Bo.UpdateStausByAdmin(otp, status);
                if(isUpdated > 0)
                {
                    return Json("Success");
                }
                else
                {
                    return Json("Some thing Went Wrong");
                }
            }
            catch (Exception e)
            {

                return Json(e.Message);
            }

        }
        public IActionResult GertProductByOTP(string otp)
        {
            var list = Bo.GetProductByOtp(otp);
            ViewBag.role = "admin";
            return View("StatusView", list);
        }

    }
}
