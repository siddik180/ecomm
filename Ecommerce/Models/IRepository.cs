﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public interface IRepository<Entity> where Entity : class
    {
        void BulkInsert(IEnumerable<Entity> entities);
        void BulkUpdate(IEnumerable<Entity> entities);
        void Delete(Entity entity);
        void DeleteById(object id);
        Entity GetById(object id);
        void Insert(Entity entity);
        IQueryable<Entity> Set();
        void Update(Entity entity);
        void Detach(IEnumerable<Entity> entities);
        void MarkAsUnModified(string propertyName);
        public void BulkDelete(IEnumerable<Entity> entities);
    }
}
