﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class Business
    {
        UnitOfWork unitOfWork;
        public Business(IHttpContextAccessor httpContextAccessor)
        {
            unitOfWork = httpContextAccessor.HttpContext.RequestServices.GetService(typeof(UnitOfWork)) as UnitOfWork;
        }
        public List<Category> GetCategories()
        {
            var rec = unitOfWork.Repository<Category>().Set().ToList();
            return rec;
        }
        public List<Productinfo> GetProducts()
        {
            var rec = unitOfWork.Repository<Productinfo>().Set().ToList();
            return rec;
        }
        public List<Product> GetOrderedProduct()
        {
            var rec = unitOfWork.Repository<Product>().Set().Where(e => e.Status != "In Cart").ToList();
            var List = new List<Product>();
            rec.GroupBy(e => e.OTP).ToList().ForEach(e => {
                var firstRec = e.ToList().First();
                List.Add(firstRec);
            });
            return List;
        }
        public List<Product> GetOutOfOrderProduct()
        {
            var rec = unitOfWork.Repository<Product>().Set().Where(e => e.Status == "Out For Delivery").ToList();
            var List = new List<Product>();
            rec.GroupBy(e => e.OTP).ToList().ForEach(e => {
                var firstRec = e.ToList().First();
                List.Add(firstRec);
            });
            return List;
        }
        public int SaveUserByMobileNumber(string MobileNumber)
        {
            var mob = MobileNumber;
            var success = -1;
            try
            {
                if (mob.Contains(" ") || mob.Contains("-"))
                {
                    mob = mob.Replace(" ", "");
                    mob = mob.Replace("-", "");
                }

                UserMaster user = new UserMaster();
                user.MobileNo = mob.Trim();
                var isExistUser = unitOfWork.Repository<UserMaster>().Set().Where(e => e.MobileNo == mob).FirstOrDefault();
                if (isExistUser == null)
                {
                    UserRoleMap map = new UserRoleMap();
                    unitOfWork.BeginTransaction();
                    unitOfWork.Repository<UserMaster>().Insert(user);
                    unitOfWork.SaveChanges();

                    map.UserId = user.Id;
                    map.RoleId = 2;
                    unitOfWork.Repository<UserRoleMap>().Insert(map);
                    unitOfWork.SaveChanges();
                    unitOfWork.Commit();
                    success = 1;
                }
                else if (isExistUser.Id > 0)
                {
                    success = 2;
                }
            }
            catch (Exception e)
            {
                unitOfWork.Rollback();
            }
            return success;

        }

        public int addToCart(string outs, string pno)
        {

            var resul = -1;
            try
            {
                Productinfo pinfo = new Productinfo();
                pinfo = unitOfWork.Repository<Productinfo>().Set().Where(e => e.ProductName == outs).FirstOrDefault();
                var isExistProduct = unitOfWork.Repository<Productinfo>().Set().Where(e => e.ProductName == outs).FirstOrDefault();
                if (isExistProduct != null)
                {

                    Product prod = new Product();
                    unitOfWork.BeginTransaction();
                    prod.ProductID = pinfo.ProductId;
                    prod.ProductName = pinfo.ProductName;
                    prod.Price = pinfo.Price;
                    prod.OrderDate = DateTime.Today;
                    prod.Status = "In Cart";
                    prod.Mobilenum = pno;
                    unitOfWork.Repository<Product>().Insert(prod);
                    unitOfWork.SaveChanges();
                    unitOfWork.Commit();
                    resul = 1;

                }
            }
            catch (Exception e)
            {
                unitOfWork.Rollback();
            }
            return resul;

        }
        public string GenerateOTP()
        {
            var chars1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars1 = new char[4];
            var random1 = new Random();

            for (int i = 0; i < stringChars1.Length; i++)
            {
                stringChars1[i] = chars1[random1.Next(chars1.Length)];
            }

            var str = new String(stringChars1);
            return str;
        }
        public (int,string) UpdateStatus(string pno)
        {

            var result = -1;
            var OTP = GenerateOTP();
            try
            {
                List<Product> pupd;
                pupd = unitOfWork.Repository<Product>().Set().Where(e => e.Mobilenum == pno).ToList();
                unitOfWork.BeginTransaction();
                foreach (var rec in pupd)
                {
                    rec.Status = "Ordered";
                    rec.OTP = OTP;
                    unitOfWork.Repository<Product>().Update(rec);
                    unitOfWork.SaveChanges();

                }
                unitOfWork.Commit();
                result = 1;

            }
            catch (Exception e)
            {
                unitOfWork.Rollback();
            }
            return (result,OTP);

        }
        public int UpdateStausByAdmin(string otp, string status)
        {
            var success = -1;
            try
            {
                unitOfWork.BeginTransaction();
                var rec = unitOfWork.Repository<Product>().Set().Where(e => e.OTP == otp).ToList();
                rec.ForEach(e =>
                {
                    e.Status = status;
                    unitOfWork.Repository<Product>().Update(e);
                    unitOfWork.SaveChanges();
                });
                
                unitOfWork.Commit();
                success = 1;
            }
            catch(Exception e)
            {
                unitOfWork.Rollback();
                throw e;
            }
            return success;
        }
        public int ValidateUser(string user, string pass)
        {
            int success = -1;
            try
            {
               var rec =  unitOfWork.Repository<UserMaster>().Set().Where(e => e.MobileNo == user && pass == "Admin@123").FirstOrDefault();
                if(rec != null)
                {
                    success = 1;
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            return success;
        }
        public int ValidateSalesPerson(string user, string pass)
        {
            int success = -1;
            try
            {
                var rec = unitOfWork.Repository<UserMaster>().Set().Where(e => e.MobileNo == user && pass == "Sales@123").FirstOrDefault();
                if (rec != null)
                {
                    success = 1;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return success;
        }
        public List<Product> GetProductByOtp(string otp)
        {
            return unitOfWork.Repository<Product>().Set().Where(e => e.OTP == otp.ToLower().Trim()).ToList();
        }
    }
}
