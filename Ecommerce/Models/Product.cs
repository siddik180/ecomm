﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class Product : AuditableBase
    {
        [Key]
        public int Id { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }

        public string Price { get; set; }
        public DateTime OrderDate { get; set; }
        public string Status { get; set; }
        public string Mobilenum { get; set; }
        public string OTP { get; set; }
    }
}
