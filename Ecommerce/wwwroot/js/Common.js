﻿function textToAudio(msg = "Please, Enter Mobile Number For Login") {

    //let msg = document.getElementById("text-to-speech").value;
    let speech = new SpeechSynthesisUtterance();
    speech.lang = "en-US";

    speech.text = msg;
    speech.volume = 1;
    speech.rate = 1;
    speech.pitch = 1;
    //var ut = new SpeechSynthesisUtterance(msg);
    //ut.speak();
    window.speechSynthesis.speak(speech);

    return true;
}

function runSpeechRecognition(obj, appedEle = null) {
    //appedEle.val('');//erase the text in input field
    // get output div reference
    var output = document.getElementById("output");
    // get action element reference
    var action = document.getElementById("action");
    // new speech recognition object
    var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
    var recognition = new SpeechRecognition();
    recognition.continuous = true;

    // This runs when the speech recognition service starts
    recognition.onstart = function () {
        if (speechHint != "search" && speechHint != "track") {
            debugger;
            textToAudio("Please Enter your mobile number");
            obj.html("<small>listening, please speak...</small>");
            
        }
    };

    recognition.onspeechend = function () {
        if (speechHint != "search" && speechHint != "track") {
            obj.html("<small>stopped listening...</small>");
            if (speechHint == 'track') {
                ApplyTrack();
            }
            if (speechHint == 'search') {
                ApplySearch();
            }
           
        }
        //recognition.stop();
        // repeatSpeechListinig(obj, appedEle);
    }

    // This runs when the speech recognition service returns result
    recognition.onresult = function (event) {
        var transcript = event.results[0][0].transcript;
        var confidence = event.results[0][0].confidence;
        if (speechHint == "search" || speechHint == "track") {
            if (speechHint == "search") {
                $("#Search").val(transcript);
            }
            if (speechHint == "track") {
                if (transcript != '7' )
                $("#track").val(transcript);
            }
            if (transcript == "add" || transcript == "ad") {
                update();
            }
            else if (transcript == '1' || transcript == 'one') {
                $(".trackele").css('display', 'block');
                textToAudio("Please Enter Order Id and say 7 for get status");
                ApplyTrack();
            }
            else if (transcript == '8') {
                $("#closeOptModal").click();
                ApplySearch();
            }
            else if (transcript == '7') {
                
                textToAudio("Please wait we are getting the status");
                track();
            }
            else {
                getProduct(transcript);
            }

        } else {
            appedEle.val(transcript);
            validateMobileNumber(appedEle);
            ClearSearchHint();
        }

        setInterval(repeatSpeechListinig(obj, appedEle), 10000);
    };

    recognition.start();
}
function repeatSpeechListinig(obj, appedEle) {
    runSpeechRecognition(obj, appedEle)
    // setInterval(runSpeechRecognition(obj, appedEle), 10000); 

}
function validateMobileNumber(ele) {
    //var phoneno = /^\d{10}$/;
    //if (ele.val().trim().match(phoneno)) {
    //    save();
    //}
    //else {
    //    textToAudio("Invalid Mobile Number, Please try again")
    //    alert("Invalid Mobile Number")
    //    runSpeechRecognition($("#voice"), $("#MobileNumber"));
    //    ele.val('');
    //    return false;
    //}
    if (ele.val().trim().length < 10) {
        if (textToAudio("Invalid Mobile Number, Please try again")) {
            alert("Invalid Mobile Number");
            ClearSearchHint();
            runSpeechRecognition($("#voice"), $("#MobileNumber"));
            ele.val('');
        }

    } else {
        save();
    }

}

function phonenumber(inputtxt) {
    var phoneno = /^\d{10}$/;
    if (inputtxt.value.match(phoneno)) {
        return true;
    }
    else {
        alert("Invalid Mobile Number");
        ClearSearchHint();
        return false;
    }
}

function track() {
    var tr = $("#track").val();
    if (tr == '') {
        textToAudio("please provide order id");
        return false;
    }
    $.ajax({
        url: "/home/TrackOrder?otp=" + tr,
        type: "GET",
        success: function (res) {
            textToAudio("your product status is " + res);
            $("#trackStatus").text("your product status is" + res);
            setTimeout(function () {
                textToAudio("Say 1 for Track Order or Say 8 For Browse");
                ApplySearch();
            }, 400);
        },
        error: function (err) {
            alert(err);
        }
    });
}

function getProduct(str) {
    if (speechHint == "search") {
        var isAvail = false;
        $(".products").map(function () {
            if ($(this).val().toLowerCase().search(str.trim().toLowerCase()) > -1) {
                var i = 0;
                isAvail = true;
                var src = $(this).attr('imgsrc')
                var price = $(this).attr('price')
                var div = '<div style="display:flex;width:100%;padding: 10px 0px;border: 1px solid;margin: 10px 0px;">'
                div += '<div style="width:30%;text-align: center;">';
                div += '<img class="product-img" style="top: 9%;"src=' + src + '></div>';
                div += '<div class="right-content">';
                div += '<label style="font-size: 24px;">' + $(this).val() + '</label><br />';
                div += '<label style="font-size: 18px;font-weight: 600;">' + price + ' Rs</label><br />';
                div += '</div></div>';
                $('#cartlist').append(div);
                add(str)
                return false;
            }

        });
        if (isAvail == false) {
            textToAudio("No Product Found, Please try again");
            alert("No Product Found, Please try again");
        }
        ApplySearch();
    }
    
}

function ApplySearch() {
    debugger;
    speechHint = "search";
    runSpeechRecognition($("#Search"));
}
function ApplyTrack() {
    speechHint = "track";
    runSpeechRecognition($("#track"))
}
function ClearSearchHint() {
    speechHint = "";
}